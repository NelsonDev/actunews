<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default_home")
     */
    public function home(EntityManagerInterface $entityManager)
    {
        $articles = $entityManager->getRepository(Article::class)->findBy(['deletedAt' => null]);

        
        return $this->render('default/home.html.twig', [
            'articles' =>$articles
        ]);
    }



     /**
     * @Route("/render-categories",name="render_categories", methods={"GET"})
     * @param EntityManagerInterface $entityManager
     * @param Response
     */
    public function renderCategories(EntityManagerInterface $entityManager): Response
    {
        #Ajouter la liste des catégories dans la navBar sous forme select
        $categories = $entityManager->getRepository(Category::class)->findBy(['deletedAt' => null]);
                    
        return $this->render('rendered/nav_categories.html.twig', [
            'categories' => $categories
        ]);
    }


    #UNE AUTRE 
    /**
     * @param EntityManagerInterface $entityManager  Pas 
     * @param Response
     */
    public function renderCategoriesFooter(EntityManagerInterface $entityManager): Response
    {
        #Ajouter la liste des catégories dans la footer sous forme select
        $categories = $entityManager->getRepository(Category::class)->findBy(['deletedAt' => null]);
                    
        return $this->render('rendered/footer_categories.html.twig', [
            'categories' => $categories
        ]);
    }


}