<?php
namespace App\Controller;

use DateTime;
use App\Entity\User;
use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CategoryController extends AbstractController
{

    /**
     * @Route("/admin/archiver-category/{id}", name="soft_delete_category", methods={"GET"})
     * @param Category $category
     * @param EntityManagerInterface $EntityManager
     * @return Response $Response
     */
    public function softDeleteCategory(Category $category, EntityManagerInterface $entityManager , FlashyNotifier $flashy): Response
    {
        $category->setDeletedAt(new DateTime());
        $entityManager->persist($category);
        $entityManager->flush();
        $flashy->success('La catégorie a bien été supprimé');
        return $this->redirectToRoute('show_dashboard');
    }


    /**
     * @Route(""/admin/supprimer-category/{id}"", name="soft_delete_category", methods={"GET"})
     * @param Category $category
     * @param EntityManagerInterface $EntityManager
     * @return Response $Response
     */
    public function hardDeleteCategory(Category $category, EntityManagerInterface $entityManager , FlashyNotifier $flashy): Response
    {
        $entityManager->remove($category);
        $entityManager->flush();
        $flashy->success('La catégorie a été supprimé définition de la base de données.');
        return $this->redirectToRoute('show_dashboard');
    }

}