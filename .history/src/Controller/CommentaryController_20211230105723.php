<?php

namespace App\Controller;

use DateTime;
use App\Entity\Article;
use App\Entity\Commentary;
use App\Form\CommentaryType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommentaryController extends AbstractController
{
    
    
    /**
     * @Route("/ajouter-un-commentaire?article_id={id}", name="add_commentary", methods={"GET|POST"})
     * @param Article $article
     * @param Request $request
     * @param EntityManagerInterface $EntityManager
     * @return Response $Response
     */
    public function addCommentary(Article $article, Request $request, EntityManagerInterface $EntityManager, FlashyNotifier $flashy): Response
    {
        $commentary = new Commentary();

        $form = $this->createForm(CommentaryType::class, $commentary)
                    ->handleRequest($request);
                    
        //Nous traitons le formulaire s'il est valide
        if ($form->isSubmitted() && $form->isValid()) {
            dd($request);
            $commentary = $form->getData();
            $commentary->setAuthor($this->getUser());
            $commentary->setCreatedAt(new DateTime());
            $commentary->setArticle($article);

            $EntityManager->persist($commentary);
            $EntityManager->flush();

            $flashy->success('Vous avez ajouté un commentaire avec succès :+1:');

            return $this->redirectToRoute('show_article', [
                'id' => $article->getId()
            ]);
        }


        return $this->render('rendered/form_commentary.html.twig', [
            'form' => $form->createView(),
            'article' => $article
        ]);
    }
}