<?php
namespace App\Controller;

use DateTime;
use App\Entity\User;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends AbstractController
{
    /**
     * @Route("/inscription", name="register", methods={"GET|POST"})
     * @param Request $request
     * @return Response
     */
    public function register(Request $resquest,UserPasswordEncoderInterface $passwordHasher, EntityManagerInterface $entityManager, FlashyNotifier $flashy):Response
    {   
        # Nouvelle instance de la classe User
        $user = new User();

        #matérialisation du formulaire déclaré dans RegisterType.php
        $form = $this->createForm(RegisterType::class, $user);
        
        # La méthode handlRequest() sert à récupérer les données du formulaire
        $form->handleRequest($resquest);
        if($form->isSubmitted() && $form->isValid()){
            //La méthode getData permet de récupérer les données du formulaire.
            $user = $form->getData();
           // $user->setCreatedAt(new DateTime());
            $user->setPassword($passwordHasher->encodePassword($user, $user->getPassword()));
            //$user->setRoles(['ROLE_USER']);
            $entityManager->persist($user);
            $entityManager->flush($user);
            $flashy->success('Votre inscription a été validé !');
            return $this->redirectToRoute('app_login');

        }

        return $this->render('register/register.html.twig',[
            'form' => $form->createView()
        ]);

    }


    /**
     * @Route("/admin/supprimer/{}id", name="soft_delete_user", methods={"GET"})
     * @param User $user
     * @param EntityManagerInterface $EntityManager
     * @return Response $Response
     */
    public function softDeleteCommentary(User $user, EntityManagerInterface $entityManager , FlashyNotifier $flashy): Response
    {
        $user->setDeletedAt(new DateTime());
        $entityManager->persist($user);
        $entityManager->flush();
       
        return $this->redirectToRoute('show_dashboard');
    }

}