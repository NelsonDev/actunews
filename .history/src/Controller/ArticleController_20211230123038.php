<?php

namespace App\Controller;

use DateTime;
use App\Entity\Article;
use App\Entity\Category;
use App\Form\ArticleType;
use App\Entity\Commentary;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;


class ArticleController extends AbstractController
{
    /**
     * @Route("/admin/creer-un-article", name="create_article", methods={"GET|POST"})
     * @param Request $request
     * @return Response
     */
    public function createArticle(Request $request, SluggerInterface $slugger, EntityManagerInterface $entityManager, FlashyNotifier $flashy): Response
    {
        # Nouvelle instance de la classe article (entity)
        $article = new Article();

        # Matérialisation du formulaire déclaré dans ArticleType.php
        $form = $this->createForm(ArticleType::class, $article);

        # handleRequest() sert à récupérer les données du formulaire
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            #Ajouter le nom de l'utilisateur qui a crée l'article grace au user provider de Symfony
            $article->setAuthor($this->getUser());

            # Récupération des données du formulaire
            $article = $form->getData();

            # Récupération du fichier du formulaire
            $file = $form->get('photo')->getData();


            # Définition de l'alias grâce à slugger, basé sur le titre. Slugger supprime les espaces et les caractères indésirables.
            $article->setAlias($slugger->slug($article->getTitle()));

            if ($file) {
                $originalFileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

                $extension = '.' . $file->guessExtension();

                $safeFilename = $slugger->slug($originalFileName);

                $newFilename = $safeFilename . '-' . uniqid() . $extension;

                try {
                    $file->move('uploads_dir', $newFilename);
                    $article->setPhoto($newFilename);
                } catch (FileException $exception) {
                    $flashy->error('Une erreur a été rencontrée');
                }
            }

            # Création du conteneur et insertion en base de données grâce à Doctrine et l'outil entityManager.
            $entityManager->persist($article);

            # On vide l'entity manager des données précédement contenues.
            $entityManager->flush();

            //$this->addFlash('success', 'Vous avez créé un nouvel article !');
            $flashy->success('Vous avez créé un nouvel article !');

            # Redirection sur la page d'accueil
            return $this->redirectToRoute('default_home');
        }

        return $this->render('article/form_article.html.twig', [
            'form' => $form->createView(),
        ]);
    }



    /**
    * @Route("/admin/editer-un-article_{id}", name="edit_article", methods={"GET|POST"})
    */
    public function editArticle(Article $article, FlashyNotifier $flashy, Request $request, SluggerInterface $slugger, EntityManagerInterface $entityManager): Response
    {
        $form= $this->createForm(ArticleType::class, $article, [
            'photo' => $article->getPhoto()
        ]);

        $originalPhoto = $article->getPhoto();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article->setUpdatedAt(new DateTime());
            $article->setAlias($slugger->slug($article->getTitle()));

            $file = $form->get('photo')->getData();

            if ($file) {
                $extension = '.' . $file->guessExtension();

                $safeFilename = $article->getAlias();

                $newFilename = $safeFilename . '_' . uniqid() . $extension;

                try {
                    $file->move($this->getParameter('uploads_dir'), $newFilename);
                    $article->setPhoto($newFilename);
                } catch (FileException $exeception) {
                }
            } else {
                $article->setPhoto($originalPhoto);
            }

            $entityManager->persist($article);
            $entityManager->flush();

            $flashy->success("Votre articlce  à bien été modifé !");

            return $this->redirectToRoute('show_dashboard');
        }

        return $this->render('article/form_article.html.twig', [
            'form' => $form->createView(),
            'article' => $article
        ]);
    }


    /**
     * @Route("/voir-un-article_{id}", name="show_article", methods={"GET"})
     */
    public function showArticle(Article $article, EntityManagerInterface $entityManager): Response
    {
        $commentaries = $entityManager->getrepository(Commentary::class)->findBy(['article' => $article->getId()]);

        return $this->render('article/show_article.html.twig', [
            'article' => $article,
            'commentaries' => $commentaries
        ]);
    }


    /**
     * @Route("/{id}/voir-tous-les-articles", name="show_articles_form_category", methods={"GET"})
     */
    public function showArticlesFormCategory(Category $category, EntityManagerInterface $entityManager)
    {
        $articles = $entityManager->getrepository(Article::class)->findBy(['category' => $category->getId()]);

    }
}