<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class DashboardController extends AbstractController
{

    /**
     * @Route("/dashboard", name="show_dashboard", methods={"GET"})
     */
    public function showDashboard(EntityManagerInterface $entityManager): Response
    {
        //récupérer tous les articles, les catégories et les utilisateurs dont 
        $articles = $entityManager->getRepository(Article::class)->findBy(['deletedAt' => null]);

        $categories = $entityManager->getRepository(Category::class)->findAll();

        $users = $entityManager->getRepository(User::class)->findBy(['deletedAt' => null]);
        
        return $this->render('dashboard/showDashboard.html.twig', [
            'articles' => $articles,
            'categories' => $categories,
            'users' => $users,
        ]);
    }

}