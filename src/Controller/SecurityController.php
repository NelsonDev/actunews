<?php

namespace App\Controller;

use DateTime;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }


    /**
     * @Route("profile/editer-mon-compte_{id}", name="edit_user",  methods={"GET|POST"})
     */    
    /**
     * edit
     *
     * @param  mixed $user
     * @param  mixed $request
     * @param  mixed $entityManager
     * @param  mixed $flashy
     * @return Response
     */
    public function edit(User $user, Request $request, EntityManagerInterface $entityManager, FlashyNotifier $flashy): Response
    {
        $form = $this->createForm(RegisterType::class)
                    ->handleRequest($request);
        

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setUpdatedAt(new DateTime());

            //Eviter qu'il nous ajoute le mot de passe en claire dans la base de données
            // if (! empty($form->get('password')->getData())) {
            //     $user->setPassword($passwordHasher->encodePassword($user, $user->getPassword()));
            // }
            $entityManager->persist($user);
            $entityManager->flush();

            $flashy->success("Vos modifications ont été enregistrées !");
            $this->redirectToRoute('show_profile');

        }

        return $this->render('profile/edit_user.html.twig', [
            'form' => $form->createView(),
        ]);
    }


}