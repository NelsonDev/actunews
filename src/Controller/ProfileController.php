<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;



class ProfileController extends AbstractController
{    
    /**
     * @Route("/profifle", name="show_profile", methods={"GET"})
     * @param Request $request
     * showProfile
     *
     * @return Response
     */
    public function showProfile(): Response
    {
        return $this->render('profile/showProfile.html.twig');
    }
    
}